# ArucoMarkerTracking

This project has the necessary ROS packages for tracking Aruco Markers for pose estimation of objects.

Ubuntu version `18.04`

ROS version `melodic`

This documentation assumes that, the usb camera is calibrated and the required camera information is stored under the file name `head_camera.yaml` in the path `/home/<username>/.ros/camera_info/`. **Without the required `.yaml` file, the camera will not be able to track the markers.**

We essentially need two ROS packages, namely `usb_cam` and `aruco_ros`. These two packages can be downloaded from the corresponding git repositories from the appropriate branches as follows

```
$ cd ~/aruco_ws
$ source devel/setup.bash
$ cd src
$ git clone https://github.com/ros-drivers/usb_cam.git
$ git clone --branch melodic-devel https://github.com/pal-robotics/aruco_ros.git
$ cd ..
$ catkin_make
```

Next we need to create two custom roslaunch files to track the markers for our purpose.

The first roslaunch file name is `usb_cam_custom.launch` located in the launch folder of the `usb_cam` package. We may need to change the value of the parameter `video_device` in the launch file if the default device location is not `/dev/video0`. The complete roslaunch file is given below.

```
<launch>
  <node name="usb_cam" pkg="usb_cam" type="usb_cam_node" output="screen" >
    <param name="video_device" value="/dev/video0" />
    <param name="image_width" value="640" />
    <param name="image_height" value="480" />
    <param name="pixel_format" value="yuyv" />
    <param name="camera_frame_id" value="usb_cam" />
    <param name="io_method" value="mmap"/>
  </node>
</launch>
```

The second roslaunch file name is `aruco_marker_finder.launch` and located in the launch folder of the `aruco_ros` package. We need to set the values of the arguments `markerId` and `markerSize` with the appropriate numbers. The launch file looks as follows

```
<launch>
 
<arg name="markerId" default="0"/>
<arg name="markerSize" default="0.05"/> <!-- in meter -->
<arg name="eye" default="left"/>
<arg name="marker_frame" default="marker_frame"/>
<arg name="ref_frame" default=""/> <!-- leave empty and the pose will be published wrt param parent_name -->
<arg name="corner_refinement" default="LINES" /> <!-- NONE, HARRIS, LINES, SUBPIX -->
 
 
<node pkg="aruco_ros" type="single" name="aruco_single">
<remap from="/camera_info" to="/usb_cam/camera_info" />
<remap from="/image" to="/usb_cam/image_raw" />
<param name="image_is_rectified" value="True"/>
<param name="marker_size" value="$(arg markerSize)"/>
<param name="marker_id" value="$(arg markerId)"/>
<param name="reference_frame" value="$(arg ref_frame)"/> <!-- frame in which the marker pose will be refered -->
<param name="camera_frame" value="base_link"/>
<param name="marker_frame" value="$(arg marker_frame)" />
<param name="corner_refinement" value="$(arg corner_refinement)" />
</node>
 
</launch>
```

# Launch instructions
**Terminal-1**
```
$ roslaunch usb_cam usb_cam_custom.launch
```

**Terminal-2**
```
roslaunch aruco_ros aruco_marker_finder.launch
```

**Terminal-3**
```
$ rosrun image_view image_view image:=/aco_single/result
```




